package net.champions.roc.WaveNation.util;

import net.champions.roc.WaveNation.WaveNation;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.UUID;

public class ParticleTrail implements Runnable, Listener {

    private static HashMap<String,NPC> worldBorders;

    public ParticleTrail(){
        worldBorders = new HashMap<>();
        Bukkit.getPluginManager().registerEvents(this, WaveNation.get());
    }

    @Override
    public void run() {
        for(String key : worldBorders.keySet()){
            if(worldBorders.get(key) == null || !worldBorders.get(key).isSpawned() || worldBorders.get(key).getEntity() == null) return;
            Location loc = worldBorders.get(key).getEntity().getLocation();
            Bukkit.getPlayer(UUID.fromString(key)).spigot().playEffect(loc, Effect.COLOURED_DUST,0,0,0,0,0,2,100,50);
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        worldBorders.remove(e.getPlayer().getUniqueId().toString());
    }

    @EventHandler
    public void onKicked(PlayerKickEvent e){
        worldBorders.remove(e.getPlayer().getUniqueId().toString());
    }

    public static void sendWorldBUpdate(Player player, NPC loc){
        if(loc != null){
            worldBorders.put(player.getUniqueId().toString(), loc);
        }
    }

    public static void removeWorldB(Player player){
        worldBorders.remove(player.getUniqueId().toString());
    }
}
