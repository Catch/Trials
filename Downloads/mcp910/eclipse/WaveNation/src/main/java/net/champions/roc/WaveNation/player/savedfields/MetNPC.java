package net.champions.roc.WaveNation.player.savedfields;

import com.mongodb.BasicDBObject;
import lombok.Getter;
import lombok.Setter;

public class MetNPC {

    @Getter
    private final String displayName;

    @Getter @Setter
    private long firstMet;

    @Getter @Setter
    private int stage;

    public MetNPC(BasicDBObject object){
        this.displayName = object.getString("displayName");
        this.firstMet = object.getLong("firstMet");
        this.stage = object.getInt("stage");
    }

    public BasicDBObject toDBObject(){
        BasicDBObject object = new BasicDBObject();
        object.put("displayName", displayName);
        object.put("firstMet", firstMet);
        object.put("stage", stage);
        return object;
    }
}
