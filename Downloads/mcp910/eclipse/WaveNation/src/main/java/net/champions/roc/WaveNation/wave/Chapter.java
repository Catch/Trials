package net.champions.roc.WaveNation.wave;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.Settings;
import net.champions.roc.WaveNation.database.DatabaseEngine;
import net.champions.roc.WaveNation.player.WavePlayer;
import net.champions.roc.WaveNation.util.LocationUtils;
import net.champions.roc.WaveNation.wave.npc.WaveNPC;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class Chapter {

    private final String name, WaveName, iconDisplay, iconLore, location, queuedDisplay;

    private final int estimatedTime, queuedStage;

    //Rewards here
    private int exp;

    @Getter
    private final List<WaveNPC> npcs;

    @Getter
    private final SaveTemplate saveTemplate;

    public Chapter(BasicDBObject object) {
        this.saveTemplate = new SaveTemplate((BasicDBObject) object.get("saveTemplate"));
        this.name = object.getString("name");
        this.location = object.getString("location");
        this.WaveName = object.getString("waveName");
        this.queuedStage = object.getInt("queuedStage");
        this.iconDisplay = ChatColor.translateAlternateColorCodes('&', object.getString("iconDisplay"));
        this.queuedDisplay = object.getString("queuedDisplay");
        this.iconLore = ChatColor.translateAlternateColorCodes('&', object.getString("iconLore"));
        this.estimatedTime = object.getInt("estimatedTime");
        this.exp = object.getInt("exp");
        this.npcs = Lists.newArrayList();
        this.npcs.addAll(((List<BasicDBObject>) object.get("npcs")).stream().map(WaveNPC::new).collect(Collectors.toList()));
    }

    public static Chapter findSync(String name, String WaveName) {
        DBObject o = DatabaseEngine.getInstance().getCollection(Settings.Wave_COLL).findOne(new BasicDBObject("name", WaveName));
        for (BasicDBObject chapter : (List<BasicDBObject>) o.get("chapters")) {
            if (chapter.getString("name").equalsIgnoreCase(name)) {
                return new Chapter(chapter);
            }
        }
        return null;
    }


    public Location getLocation(){
        return LocationUtils.stringToLocation(location);
    }

    public ItemStack getIcon(WavePlayer player){
        boolean unlocked = hasUnlocked(player);
        ItemStack stack = new ItemStack(Material.WOOL,1, (short) 0, (byte) ((unlocked) ? 5 : 14));
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(iconDisplay);
        meta.setLore(Arrays.asList(iconLore, "", org.bukkit.ChatColor.YELLOW + "Estimated Time: " + org.bukkit.ChatColor.GRAY + estimatedTime,
                org.bukkit.ChatColor.GRAY + "Exp Reward: " +
                org.bukkit.ChatColor.GOLD + exp));
        stack.setItemMeta(meta);
        return stack;
    }

    public ItemStack getUnlockedIcon(WavePlayer player) {
        ItemStack stack = new ItemStack(Material.WOOL, 1, (short) 0, (byte) 5);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(iconDisplay);
        meta.setLore(Arrays.asList(iconLore, "", org.bukkit.ChatColor.GRAY + "Estimated Time: " + org.bukkit.ChatColor.YELLOW + estimatedTime,
                org.bukkit.ChatColor.GRAY + "Exp Reward: " +
                        org.bukkit.ChatColor.GOLD + exp));
        stack.setItemMeta(meta);
        return stack;
    }

    public boolean hasUnlocked(WavePlayer player){
        if(player == null) return false;
        return player.getUnlockedChapters().contains(name);
    }


    public BasicDBObject toDBObject(){
        BasicDBObject object = new BasicDBObject();
        object.put("name", name);
        object.put("waveName", WaveName);
        object.put("iconDisplay", iconDisplay);
        object.put("queuedStage", queuedStage);
        object.put("iconLore", iconLore);
        object.put("queuedDisplay", queuedDisplay);
        object.put("estimatedTime", estimatedTime);
        object.put("exp" , exp);
        object.put("location", location);
        BasicDBList npcList = npcs.stream().map(WaveNPC::toDBObject)
                .collect(Collectors.toCollection(() -> new BasicDBList()));

        object.put("npcs", npcList);
        return object;
    }
}
