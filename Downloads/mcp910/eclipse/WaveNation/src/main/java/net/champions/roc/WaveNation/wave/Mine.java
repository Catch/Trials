package net.champions.roc.WaveNation.wave;

import com.mongodb.BasicDBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.util.Cuboid;
import net.champions.roc.WaveNation.util.LocationUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.Iterator;

public class Mine {


    private final String location1, location2, material;

    @Getter
    private final String name;


    public Mine(BasicDBObject object) {
        this.name = object.getString("name");
        this.location1 = object.getString("location1");
        this.location2 = object.getString("location2");
        this.material = object.getString("material");
    }

    public Cuboid getCuboid(Player player) {
        return new Cuboid(getLocation1(player), getLocation2(player));
    }

    public void reset(Player player){
        Cuboid cuboid = getCuboid(player);
        Iterator<Block> blockIterator = cuboid.iterator();
        while(blockIterator.hasNext()){
            blockIterator.next().setType(getMaterial());
        }
    }

    public Location getLocation1(Player player) {
        World world = player.getWorld();
        Location loc = LocationUtils.stringToLocation(location1);
        loc.setWorld(world);
        return loc;
    }

    public Location getLocation2(Player player) {
        World world = player.getWorld();
        Location loc = LocationUtils.stringToLocation(location2);
        loc.setWorld(world);
        return loc;
    }

    public Material getMaterial(){
        return Material.valueOf(material);
    }

    public BasicDBObject toDBObject() {
        BasicDBObject object = new BasicDBObject();
        object.put("name", name);
        object.put("location1", location1);
        object.put("location2", location2);
        object.put("material", material);
        return object;
    }
}
