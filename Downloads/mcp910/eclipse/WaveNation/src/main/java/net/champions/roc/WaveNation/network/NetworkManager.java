package net.champions.roc.WaveNation.network;

import com.google.common.collect.Lists;
import lilypad.client.connect.api.event.EventListener;
import lilypad.client.connect.api.event.MessageEvent;
import net.champions.roc.WaveNation.Settings;
import net.champions.roc.WaveNation.WaveNation;
import net.champions.roc.WaveNation.util.WorldUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class NetworkManager {

    @EventListener
    public void onMessage(MessageEvent messageEvent) {
        String sender = messageEvent.getSender();
        String channel = messageEvent.getChannel();
        try {
            if (WaveNation.get().isHub()) {
                //Received in Hub, send player -> instance server
                if (channel.equalsIgnoreCase("wavehandshakedone")) {
                    handleHub(sender, messageEvent.getMessageAsString());
                }
            } else {
                //Received in Instance Server, hub -> instance server and eventually ^
                if (channel.equalsIgnoreCase("wavehandshake")) {
                    handleInstance(sender, messageEvent.getMessageAsString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleInstance(String sender, String message) {
        //Parse the short data to maintain a fat pipeline
        List<String> data = Arrays.asList(message.split(":"));
        String uuid = data.get(0);
        String wave = data.get(1);

        //Solution is world #, we start at 1
        int solution = 1;
        int mapSize = WaveNation.get().getPlayerManager().getPlayerWorlds().size();

        if (mapSize != 0) {
            //Store all the taken world #'s in an array and sort
            List<Integer> takenWorld = Lists.newArrayList();
            for (String world : WaveNation.get().getPlayerManager().getPlayerWorlds().values()) {
                if (!world.startsWith(wave)) continue;
                int takenNum = Integer.valueOf(world.substring(world.length() - 1));
                takenWorld.add(takenNum);
            }
            boolean running = true;
            int y = 1;
            while (running) {
                if (!takenWorld.contains(y)) {
                    solution = y;
                    running = false;
                    break;
                }
                y++;
            }
        }
        //We got the solution so lets reserve the world and player
        String reservedWorld = wave + solution;
        WaveNation.get().getPlayerManager().getPlayerWorlds().put(uuid, reservedWorld);

        //If we need to generate a new world, then let's do it
        if (Bukkit.getWorld(reservedWorld) == null) {
            String base = WaveNation.get().getServer().getWorldContainer().getAbsolutePath() + "/" + wave + "-base";
            final File newLoc = new File(WaveNation.get().getServer().getWorldContainer().getAbsolutePath() + "/" + reservedWorld);
            WorldUtil.copyDir(new File(base), newLoc);
            Bukkit.getScheduler().runTask(WaveNation.get(), () -> WorldUtil.createWorld(reservedWorld));
        }
        //We finished creating and loading the world for the player, lets send them a finished message
        WaveNation.get().getLilyEssentials().request(getServer(sender), "wavehandshakedone", uuid);
    }

    private void handleHub(String sender, String message) {
        //The instance server finished loading, were in the hub
        Player p = Bukkit.getPlayer(UUID.fromString(message));//UUID
        if (p == null || p.getItemInHand() == null) {
            System.out.print("Cannot find Player to TP: " + message);
            //TODO delete there playerWorld, send a packet or wait for restart
            return;
        }
        p.sendMessage(Settings.PREFIX + ChatColor.GREEN + "Sending you to the instance now!");
        WaveNation.get().getLilyEssentials().redirectRequest(sender, p);
    }

    //Array list, what lilypad accepts (facepalm)
    public static ArrayList<String> getServer(String... servers) {
        ArrayList<String> list = new ArrayList<>();
        for (String server : servers) {
            list.add(server);
        }
        return list;
    }
}
