package net.champions.roc.WaveNation;

import org.bukkit.ChatColor;

public class Settings {

    public static final String PREFIX = ChatColor.AQUA + "[TrialNation] " + ChatColor.GREEN;

    public static final String HOST = "localhost";
    public static final int PORT = 27017;

    public static final String DB = "wavenation";
    public static final String Wave_COLL = "waves";
    public static final String PLAYER_COLL = "players";

    public static final int WORLD_COUNT = 2;
}
