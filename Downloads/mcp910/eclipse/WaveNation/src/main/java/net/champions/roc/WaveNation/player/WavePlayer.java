package net.champions.roc.WaveNation.player;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import lombok.Getter;
import lombok.Setter;
import net.champions.roc.WaveNation.WaveNation;
import net.champions.roc.WaveNation.player.savedfields.MetNPC;
import net.champions.roc.WaveNation.player.savedfields.QueuedNPC;
import net.champions.roc.WaveNation.player.savedfields.SavedNPCLoc;
import net.champions.roc.WaveNation.wave.npc.WaveNPC;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class WavePlayer {

    //An complex json pojo

    @Getter
    private List<String> unlockedChapters;

    private final String uuid;

    @Getter
    private String lastName;

    @Getter
    private String currentWave;

    @Getter
    private String currentChapter;

    @Getter @Setter
    private List<MetNPC> metNPCs;

    @Getter @Setter
    private List<QueuedNPC> queuedNPCs;

    @Getter @Setter
    private List<SavedNPCLoc> NPClocs;

    @Getter @Setter
    private BasicDBObject metFights;

    //Returns the pojo only, use the player manager class to update and retrieve
    public WavePlayer(BasicDBObject object) {
        this.unlockedChapters = (List<String>) object.get("unlockedChapters");
        this.uuid = object.getString("uuid");
        this.currentWave = object.getString("currentWave");
        this.currentChapter = object.getString("currentChapter");
        this.metNPCs = Lists.newArrayList();
        this.queuedNPCs = Lists.newArrayList();
        this.NPClocs = Lists.newArrayList();
        this.NPClocs.addAll(((List<BasicDBObject>) object.get("NPCLocs")).stream().map(SavedNPCLoc::new).collect(Collectors.toList()));
        this.queuedNPCs.addAll(((List<BasicDBObject>) object.get("queuedNPCS")).stream().map(QueuedNPC::new).collect(Collectors.toList()));
        this.metNPCs.addAll(((List<BasicDBObject>) object.get("metNPCS")).stream().map(MetNPC::new).collect(Collectors.toList()));
        this.metFights = (BasicDBObject) object.get("metFights");
        this.lastName = object.getString("lastName");
    }

    //Only used for creating new players!
     WavePlayer(Player newPlayer) {
        this.unlockedChapters = Lists.newArrayList();
        this.uuid = newPlayer.getUniqueId().toString();
        this.currentWave = null;
        this.currentChapter = null;
        this.metNPCs = Lists.newArrayList();
        this.queuedNPCs = Lists.newArrayList();
        this.NPClocs = Lists.newArrayList();
        this.metFights = new BasicDBObject();
        this.lastName = newPlayer.getName();
    }

    public boolean metStage(String display, int stage){
        for (MetNPC n : metNPCs) {
            if (ChatColor.stripColor(n.getDisplayName()).equalsIgnoreCase(ChatColor.stripColor(display))) {
                if(n.getStage() >= stage){
                    return true;
                }
            }
        }
        return false;
    }

    public Integer getUnMetStage(WaveNPC WaveNPC){
        MetNPC npc = getMetNPC(WaveNPC.getDisplayName());
        if(npc == null) return 0;
        for(int x = 0 ; x < WaveNPC.getStages().size(); x++){
            if(x > npc.getStage()){
                return x;
            }
        }
        return null;
    }

    public Boolean hasQueuedNPC(String display, Integer stage){
        if(stage == null) return null;
        for(QueuedNPC npc : queuedNPCs){
            if(ChatColor.stripColor(npc.getDisplayName())
                    .equalsIgnoreCase(ChatColor.stripColor(display))){
                if(stage == npc.getStage()){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasMet(String npcDisplay) {
        if (npcDisplay.equalsIgnoreCase("") || npcDisplay.equalsIgnoreCase(" ")) return true;
        if (metNPCs == null) return false;
        for (MetNPC n : metNPCs) {
            if (ChatColor.stripColor(n.getDisplayName())
                    .equalsIgnoreCase(ChatColor.stripColor(npcDisplay))) {
                return true;
            }
        }
        return false;
    }

    public void setCurrentChapter(String currentChapter) {
        this.currentChapter = currentChapter;
    }

    public void setCurrentWave(String currentWave) {
        this.currentWave = currentWave;
    }

    public MetNPC getMetNPC(String npcDisplay) {
        if (metNPCs == null) return null;
        for (MetNPC n : metNPCs) {
            if (ChatColor.stripColor(n.getDisplayName())
                    .equalsIgnoreCase(ChatColor.stripColor(npcDisplay))) {
                return n;
            }
        }
        return null;
    }

    public void setNPClocs(List<SavedNPCLoc> NPClocs) {
        this.NPClocs = NPClocs;
    }

    public UUID getUniqueId() {
        return UUID.fromString(uuid);
    }

    //I probably made this complex
    public String getCurrentObjective() {
        //Get the latest npc, objective is hooked with it
        String keySave = "";
        long valueSave = 1;
        Integer stage = null;
        boolean npc = false;
        for (MetNPC metNPC : metNPCs) {
            if (metNPC.getFirstMet() > valueSave) {
                keySave = metNPC.getDisplayName();
                valueSave = metNPC.getFirstMet();
                stage = metNPC.getStage();
                npc = true;
            }
        }
        for (String key : metFights.keySet()) {
            if (metFights.getLong(key) > valueSave) {
                keySave = key;
                valueSave = metFights.getLong(key);
                npc = false;
            }
        }
        if (stage == null || keySave.equalsIgnoreCase("") || valueSave == 1) return "Talk to Nearest NPC";
        if (npc) {
            return WaveNation.get().getWaveManager().
                    getWaveNPC(keySave).getStage(stage)
                    .getCurrentObjective();
        } else {
            return WaveNation.get().getWaveManager().getFightLocation(keySave).getCurrentObjective();
        }
    }

    //This too
    public Location getCurrentLocationObjective(Player player) {
        //Get the latest npc, objective is hooked with it
        String keySave = "";
        long valueSave = 1;
        boolean npc = false;
        Integer stage = null;
        for (MetNPC metNPC : metNPCs) {
            if (metNPC.getFirstMet() > valueSave) {
                keySave = metNPC.getDisplayName();
                valueSave = metNPC.getFirstMet();
                stage = metNPC.getStage();
                npc = true;
            }
        }
        for (String key : metFights.keySet()) {
            if (metFights.getLong(key) > valueSave) {
                keySave = key;
                valueSave = metFights.getLong(key);
                npc = false;
            }
        }
        if (keySave.equalsIgnoreCase("") || valueSave == 1 || stage == null) return null;
        if (npc) {
            return WaveNation.get().getWaveManager().
                    getWaveNPC(keySave).getStage(stage)
                    .getCurrentObjectiveLocation(player);
        } else {
            return WaveNation.get().getWaveManager().getFightLocation(keySave)
                    .getCurrentObjectiveLocation(player);
        }
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BasicDBObject toDBObject() {
        BasicDBObject object = new BasicDBObject();

        BasicDBList list = unlockedChapters.stream()
                .collect(Collectors.toCollection(BasicDBList::new));
        object.put("unlockedChapters", list);

        object.put("uuid", uuid);
        object.put("currentWave", currentWave);
        object.put("currentChapter", currentChapter);

        BasicDBList mn = metNPCs.stream().map(MetNPC::toDBObject)
                .collect(Collectors.toCollection(() -> new BasicDBList()));
        object.put("metNPCS", mn);

        BasicDBList q = queuedNPCs.stream().map(QueuedNPC::toDBObject)
                .collect(Collectors.toCollection(() -> new BasicDBList()));
        object.put("queuedNPCS", q);

        BasicDBList l = NPClocs.stream().map(SavedNPCLoc::toDBObject)
                .collect(Collectors.toCollection(() -> new BasicDBList()));
        object.put("NPCLocs", l);

        object.put("metFights", metFights);
        object.put("lastName", lastName);
        return object;
    }
}
