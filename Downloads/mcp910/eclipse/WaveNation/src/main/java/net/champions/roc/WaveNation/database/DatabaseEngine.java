package net.champions.roc.WaveNation.database;

import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import net.champions.roc.WaveNation.Settings;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DatabaseEngine {

    //Instance
    private static DatabaseEngine instance;

    //Client
    private MongoClient client;

    //Threading
    private final ExecutorService pool;

    public DatabaseEngine() {
        this.instance = this;
        this.pool = Executors.newCachedThreadPool();

        try {
            this.client = new MongoClient(Settings.HOST, Settings.PORT);
            System.out.print("Connected to MongoDB");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static DatabaseEngine getInstance() {
        return instance;
    }

    public DBCollection getCollection(String name) {
        return client.getDB(Settings.DB).getCollection(name);
    }

    public ExecutorService getPool() {
        return pool;
    }

}
