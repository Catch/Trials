package net.champions.roc.WaveNation.player;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.Settings;
import net.champions.roc.WaveNation.WaveNation;
import net.champions.roc.WaveNation.database.Callback;
import net.champions.roc.WaveNation.database.DatabaseEngine;
import net.champions.roc.WaveNation.player.savedfields.MetNPC;
import net.champions.roc.WaveNation.player.savedfields.QueuedNPC;
import net.champions.roc.WaveNation.player.savedfields.SavedNPCLoc;
import net.champions.roc.WaveNation.util.LocationUtils;
import net.champions.roc.WaveNation.util.Title;
import net.champions.roc.WaveNation.util.WorldUtil;
import net.champions.roc.WaveNation.wave.Chapter;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.*;
import org.inventivetalent.bossbar.BossBarAPI;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Getter
public class PlayerManager implements Listener {

    //Database methods are java synchronized; volatile for thread safe

    private volatile HashMap<String, String> playerWorlds;

    private volatile ConcurrentHashMap<String, WavePlayer> players;

    private final Title title;

    public PlayerManager() {
        //One title for everything;less mem/cpu to create this var every join
        this.title = new Title(ChatColor.AQUA + "Welcome to WaveNation!",
                ChatColor.YELLOW + "Get ready to conquer some quests!");
        title.setTimingsToSeconds();
        title.setStayTime(2);
        title.setFadeInTime(2);
        title.setFadeOutTime(2);

        this.players = new ConcurrentHashMap<>();
        if (!WaveNation.get().isHub())
            this.playerWorlds = new HashMap<>();

        Bukkit.getPluginManager().registerEvents(this, WaveNation.get());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLogin(PlayerLoginEvent e) {
        //Async thread
        getPlayerAsyncCall(e.getPlayer().getUniqueId(), player -> {
            if (player == null) {
                //Create the new player
                System.out.print("Creating a new player: " + e.getPlayer().getName());
                WavePlayer newPlayer = new WavePlayer(e.getPlayer());
                //Sync db call since were already in a thread
                insertPlayerDBSync(newPlayer);
                if (!WaveNation.get().isHub()) WaveNation.get().getScoreboardManager().update(newPlayer, e.getPlayer());
                players.put(e.getPlayer().getUniqueId().toString(), newPlayer);
                return;
            }
            System.out.print("Found player: " + e.getPlayer().getName());
            player.setLastName(e.getPlayer().getName());
            //Scoreboard manager runs this sync
            if (!WaveNation.get().isHub()) WaveNation.get().getScoreboardManager().update(player, e.getPlayer());
            players.put(e.getPlayer().getUniqueId().toString(), player);
        });
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        //Defaults
        e.getPlayer().setGameMode(GameMode.ADVENTURE);//Survival?
        e.getPlayer().setHealth(e.getPlayer().getMaxHealth());
        e.getPlayer().setFoodLevel(20);
        //Cosmetics
        title.send(e.getPlayer());
        BossBarAPI.setMessage(e.getPlayer(), ChatColor.YELLOW + "" + ChatColor.BOLD + "CHAMPIONS: " + ChatColor.AQUA + "ip", 100);

        //Make sure were in instance server code
        if (WaveNation.get().isHub()) return;
        if (!playerWorlds.containsKey(e.getPlayer().getUniqueId().toString())) {
            //This shouldn't happen...
            e.getPlayer().sendMessage(Settings.PREFIX + ChatColor.RED + "Your not in the player world list! Contact an admin!");
            WaveNation.get().getLilyEssentials().redirectRequest("hub", e.getPlayer());
        }

        DatabaseEngine.getInstance().getPool().submit(() -> {
            Chapter chap = Chapter.findSync(getPlayer(e.getPlayer().getUniqueId()).getCurrentChapter(), getPlayer(e.getPlayer().getUniqueId()).getCurrentWave());
            Bukkit.getScheduler().runTask(WaveNation.get(), () -> {
                Location loc = chap.getLocation().clone();
                loc.setWorld(Bukkit.getWorld(playerWorlds.get(e.getPlayer().getUniqueId().toString())));
                e.getPlayer().teleport(loc);
                WavePlayer player = getPlayer(e.getPlayer().getUniqueId());
                for (SavedNPCLoc npcLoc : player.getNPClocs()) {
                    //Setup npc's locations from saved template
                    for (NPC npc : CitizensAPI.getNPCRegistry()) {
                        npcLoc.getLocation(e.getPlayer()).getChunk().load();
                        if (!npc.isSpawned()) {
                            npc.spawn(npcLoc.getLocation(e.getPlayer()));
                        }
                        if (ChatColor.stripColor(npc.getName()).equalsIgnoreCase(ChatColor.stripColor(npcLoc.getDisplayName()))
                                && npc.getBukkitEntity().getLocation().getWorld().getName()
                                .equalsIgnoreCase(e.getPlayer().getWorld().getName())) {
                            e.getPlayer().getWorld().loadChunk(npc.getBukkitEntity().getLocation().getChunk());
                            npc.teleport(npcLoc.getLocation(e.getPlayer()), PlayerTeleportEvent.TeleportCause.PLUGIN);
                        }
                    }
                }
            });
        });
    }

    private void saveExit(PlayerEvent e) {
        //Save to the database and remove player from map when confirmed
        savePlayerToDB(e.getPlayer().getUniqueId(), getPlayer(e.getPlayer().getUniqueId()), aBoolean -> {
            players.remove(e.getPlayer().getUniqueId().toString());
        });
        //End conversations
        if (e.getPlayer().hasMetadata("inConversation")) {
            e.getPlayer().removeMetadata("inConversation", WaveNation.get());
        }
        if (!WaveNation.get().isHub()) {
            //Delete there data on instance server
            new File(e.getPlayer().getWorld().getWorldFolder().getPath().toString()
                    + "/players/" + e.getPlayer().getDisplayName() + ".dat").delete();

            if (!playerWorlds.containsKey(e.getPlayer().getUniqueId().toString())) return;

            //Reset there world
            String world = playerWorlds.get(e.getPlayer().getUniqueId().toString());
            String playerUUID = e.getPlayer().getUniqueId().toString();
            Bukkit.getWorld(world).getEntities().forEach(entity -> {
                if (CitizensAPI.getNPCRegistry().isNPC(entity)) {
                    CitizensAPI.getNPCRegistry().getNPC(entity).destroy();
                } else {
                    entity.remove();
                }
            });
            WorldUtil.pasteNPCS(world);
            playerWorlds.remove(playerUUID);
            //World is now usable for anyone else
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        saveExit(e);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onKick(PlayerKickEvent e) {
        e.setLeaveMessage(null);
        saveExit(e);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (WaveNation.get().isHub()) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onSpeak(AsyncPlayerChatEvent e) {
        if (WaveNation.get().isHub()) return;
        e.getPlayer().sendMessage(ChatColor.GRAY + "You may not speak during quests!");
        e.setCancelled(true);
    }

    public synchronized WavePlayer getPlayer(UUID uuid) {
        if (players.containsKey(uuid.toString())) {
            return players.get(uuid.toString());
        }
        return null;
    }

    public synchronized void getPlayerAsyncCall(UUID uuid, Callback<WavePlayer> callback) {
        if (players.containsKey(uuid.toString())) {
            Bukkit.getScheduler().runTaskAsynchronously(WaveNation.get(), () ->
                    callback.call(players.get(uuid.toString())));
            return;
        }
        DatabaseEngine.getInstance().getPool().submit(() -> {
            DBObject o = DatabaseEngine.getInstance().getCollection(Settings.PLAYER_COLL)
                    .findOne(new BasicDBObject("uuid", uuid.toString()));
            if (o == null) {
                callback.call(null);
                return;
            }
            WavePlayer player = new WavePlayer((BasicDBObject) o);
            //Async call
            callback.call(player);
        });
    }

    public synchronized void savePlayer(UUID uuid, WavePlayer toSave) {
        players.put(uuid.toString(), toSave);
    }

    public synchronized void savePlayerToDB(UUID uuid, WavePlayer toSave, Callback<Boolean> callback) {
        DatabaseEngine.getInstance().getPool().submit(() -> {
            DatabaseEngine.getInstance().getCollection(Settings.PLAYER_COLL)
                    .update(new BasicDBObject("uuid", uuid.toString()), toSave.toDBObject());
            callback.call(true);
        });
    }

    public synchronized void addMetNPC(UUID uuid, String display) {
        WavePlayer player = getPlayer(uuid);
        if (player == null) {
            return;
        }
        List<MetNPC> metNPCs = player.getMetNPCs();
        for (int x = 0; x < metNPCs.size(); x++) {
            MetNPC npc = metNPCs.get(x);
            //Already added met npc, so increment it to next stage
            if (ChatColor.stripColor(npc.getDisplayName()).equalsIgnoreCase(ChatColor.stripColor(display))) {
                npc.setStage(npc.getStage() + 1);
                npc.setFirstMet(System.currentTimeMillis());
                return;
            }
        }
        metNPCs.add(new MetNPC(new BasicDBObject("displayName", display)
                .append("firstMet", System.currentTimeMillis()).append("stage", 0)));
        player.setMetNPCs(metNPCs);
        savePlayer(uuid, player);
    }

    public synchronized void addNPCQueue(UUID uuid, String display, Integer stage) {
        WavePlayer player = getPlayer(uuid);
        if (player == null) {
            return;
        }
        System.out.print(1);
        List<QueuedNPC> queuedNPCs = player.getQueuedNPCs();
        for (QueuedNPC npc : queuedNPCs) {
            System.out.print(2);
            if (ChatColor.stripColor(npc.getDisplayName())
                    .equalsIgnoreCase(ChatColor.stripColor(display)) && stage == npc.getStage()) {
                return;
            }
        }
        System.out.print(3);
        queuedNPCs.add(new QueuedNPC(new BasicDBObject("displayName", display).append("stage", stage)));
        player.setQueuedNPCs(queuedNPCs);
        savePlayer(uuid, player);
    }

    public synchronized void updateNPCLob(UUID uuid, String display, Location loc) {
        WavePlayer player = getPlayer(uuid);
        if (player == null) {
            return;
        }
        List<SavedNPCLoc> savedNPCLocs = player.getNPClocs();
        for (int x = 0; x < savedNPCLocs.size(); x++) {
            SavedNPCLoc npc = savedNPCLocs.get(x);
            if (ChatColor.stripColor(npc.getDisplayName())
                    .equalsIgnoreCase(ChatColor.stripColor(display))) {
                npc.setLocation(LocationUtils.locationToString(loc));
                player.setNPClocs(savedNPCLocs);
                return;
            }
        }
        System.out.print(3);
        savedNPCLocs.add(new SavedNPCLoc(new BasicDBObject("displayName", display).append("location",
                LocationUtils.locationToString(loc))));
        player.setNPClocs(savedNPCLocs);
        savePlayer(uuid, player);
    }

    public synchronized void addMetFight(UUID uuid, String name) {
        WavePlayer player = getPlayer(uuid);
        if (player == null) {
            return;
        }
        player.getMetFights().append(name, System.currentTimeMillis());
        savePlayer(uuid, player);
    }

    public synchronized void removeNPCFromQueue(UUID uuid, String display, Integer stage) {
        WavePlayer player = getPlayer(uuid);
        if (player == null) {
            return;
        }
        List<QueuedNPC> queuedNPC = player.getQueuedNPCs();
        for (int x = 0; x < player.getQueuedNPCs().size(); x++) {
            QueuedNPC npc = player.getQueuedNPCs().get(x);
            if (ChatColor.stripColor(display).
                    equalsIgnoreCase(ChatColor.stripColor(npc.getDisplayName()))) {
                if (npc.getStage() == stage.intValue()) {
                    queuedNPC.remove(npc);
                }
            }
        }
        player.setQueuedNPCs(queuedNPC);
        savePlayer(uuid, player);
    }

    private synchronized void insertPlayerDB(WavePlayer newPlayer) {
        DatabaseEngine.getInstance().getPool().submit(() ->
                DatabaseEngine.getInstance().getCollection(Settings.PLAYER_COLL).insert(newPlayer.toDBObject()));
    }

    private synchronized void insertPlayerDBSync(WavePlayer newPlayer) {
        DatabaseEngine.getInstance().getCollection(Settings.PLAYER_COLL).insert(newPlayer.toDBObject());
    }
}
