package net.champions.roc.WaveNation.player.savedfields;

import com.mongodb.BasicDBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.util.LocationUtils;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class SavedNPCLoc {

    @Getter
    private final String displayName;

    private String location;

    public SavedNPCLoc(BasicDBObject object){
        this.displayName = object.getString("displayName");
        this.location = object.getString("location");
    }

    public Location getLocation(Player player) {
        Location loc = LocationUtils.stringToLocation(location);
        loc.setWorld(player.getWorld());
        return loc;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public BasicDBObject toDBObject(){
        BasicDBObject object = new BasicDBObject();
        object.put("displayName", displayName);
        object.put("location", location);
        return object;
    }
}
