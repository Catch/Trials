package net.champions.roc.WaveNation.util;

import net.champions.roc.WaveNation.WaveNation;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

public class ActionBar implements Runnable, Listener {

    private static HashMap<String,String> titles;
    private static ItemStack trigger;

    public ActionBar(){
        titles = new HashMap<>();
        trigger= new ItemStack(Material.COMPASS, 1);
        ItemMeta meta = trigger.getItemMeta();
        meta.setDisplayName(ChatColor.BLUE + "Current Objective Location");
        meta.setLore(Arrays.asList(
                ChatColor.YELLOW + "Use this to find the location",
                ChatColor.YELLOW + "of your next objective!",
                ChatColor.YELLOW + "May be incorrect sometimes!"
        ));
        trigger.setItemMeta(meta);
        Bukkit.getPluginManager().registerEvents(this, WaveNation.get());
    }

    @Override
    public void run() {
        for(String key : titles.keySet()){
            sendAB(Bukkit.getPlayer(UUID.fromString(key)), titles.get(key));
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        titles.remove(e.getPlayer().getUniqueId().toString());
    }

    @EventHandler
    public void onKicked(PlayerKickEvent e){
        titles.remove(e.getPlayer().getUniqueId().toString());
    }

    public static void sendActionBar(Player player, String message, Location location) {
        titles.put(player.getUniqueId().toString(), message);
        if (!player.getInventory().contains(Material.COMPASS)) {
            player.getInventory().addItem(trigger);
            player.sendMessage(ChatColor.GREEN + "A compass has been added to your inventory!");
        }
        if(location == null){
            System.out.print("location objective null");
            return;
        }
        player.sendMessage(ChatColor.DARK_GRAY + "Objective and Compass updated!");
        Location loc = location.clone();
        loc.setWorld(player.getWorld());
        player.setCompassTarget(loc);

    }

    private void sendAB(Player player, String message){
        CraftPlayer p = (CraftPlayer) player;
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);
    }
}
