package net.champions.roc.WaveNation.wave.mob;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.WaveNation;
import net.champions.roc.WaveNation.player.WavePlayer;
import net.champions.roc.WaveNation.util.LocationUtils;
import net.champions.roc.WaveNation.util.Title;
import net.elseland.xikage.MythicMobs.MythicMobs;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class FightLocation {

    @Getter
    private final String requiredNPCdisplay, currentObjective, locationObjective, queuedDisplay;

    private final String location;

    @Getter
    private final String name;

    @Getter
    private final int radius, queuedStage, requiredStage;

    @Getter
    private final List<WaveMob> mythicMobs;

    @Getter
    private final BasicDBObject speech;

    private static final Title title;

    static {
        title = new Title(org.bukkit.ChatColor.RED + "Be the champion!");
        title.setTimingsToSeconds();
        title.setSubtitle(org.bukkit.ChatColor.YELLOW + "GOOD LUCK!");
        title.setStayTime(2);
    }

    public FightLocation(BasicDBObject object) {
        this.requiredStage = object.getInt("requiredStage");
        this.queuedStage = object.getInt("queuedStage");
        this.queuedDisplay = object.getString("queuedDisplay");
        this.requiredNPCdisplay = object.getString("requiredNPCdisplay");
        this.location = object.getString("location");
        this.name = object.getString("name");
        this.radius = object.getInt("radius");
        this.speech = (BasicDBObject) object.get("speech");
        this.mythicMobs = Lists.newArrayList();
        this.currentObjective = object.getString("currentObjective");
        this.locationObjective = object.getString("locationObjective");
        this.mythicMobs.addAll(((List<BasicDBObject>) object.get("mythicMobs")).stream().map(WaveMob::new).collect(Collectors.toList()));
    }

    //Called when player is within proximity of this mob
    public void activate(Player player) {
        WavePlayer WavePlayer = WaveNation.get().getPlayerManager().getPlayer(player.getUniqueId());
        if (WavePlayer == null) return;

        BasicDBObject o = WavePlayer.getMetFights();
        if (o.keySet().contains(name)) {
            return;
        }

        if (!WavePlayer.metStage(requiredNPCdisplay
                , requiredStage)) {
            return;
        }
        //Player never met this mob

        boolean hasMet = WavePlayer.hasMet(requiredNPCdisplay);
        if (!hasMet) return;
        //If we met the npc required

        //Cosmetics
        title.send(player);
        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 80, 50));
        player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 80, 50));
        player.playSound(player.getLocation(), Sound.ENDERDRAGON_GROWL, 1, 1);

        WaveNation.get().getPlayerManager().addMetFight(player.getUniqueId(), name);

        //Track down players mobs' to fight
        HashMap<FightLocation, List<String>> insert = new HashMap<>();
        List<String> mobs = Lists.newArrayList();
        for (WaveMob mob : mythicMobs) {
            Location spawn = mob.getLocation().clone();
            spawn.setWorld(player.getWorld());
            player.spigot().playEffect(spawn, Effect.COLOURED_DUST, 0, 1, 2, 2, 2, 5, 100, 50);
            player.spigot().playEffect(spawn, Effect.EXPLOSION, 0, 1, 2, 2, 2, 5, 10, 50);
            Entity e = MythicMobs.plugin.getAPI().getMobAPI().spawnMythicMob(mob.getMythicName(), spawn);
            mobs.add(e.getUniqueId().toString());
        }
        insert.put(this, mobs);
        WaveNation.get().getWaveManager().getCurrentMobs().put(player.getUniqueId().toString(), insert);

        //Regular speech stuff
        player.setMetadata("inConversation", new FixedMetadataValue(WaveNation.get(), "inConversation"));
        int count = 0;
        for (String s : speech.keySet()) {
            int sec = Integer.valueOf(s);
            final int finalCount = count;
            Bukkit.getScheduler().runTaskLater(WaveNation.get(), () -> {
                if (Bukkit.getPlayerExact(player.getName()) == null || !player.hasMetadata("inConversation")) {
                    return;
                }
                String text = speech.getString(s);
                if (text.contains("<me>")) {
                    String newText = text.replace("<me>", "");
                    player.sendMessage(org.bukkit.ChatColor.GRAY + "[" + ChatColor.AQUA + "You" + ChatColor.GRAY + "] -> [" + ChatColor.AQUA + "Enemy" + ChatColor.GRAY +
                            "] " + ChatColor.DARK_GREEN + ChatColor.ITALIC + ChatColor.translateAlternateColorCodes('&', newText));
                } else {
                    player.sendMessage(org.bukkit.ChatColor.GRAY + "[" + ChatColor.AQUA + "Enemy" + ChatColor.GRAY + "] -> [" + ChatColor.AQUA + "You" + ChatColor.GRAY +
                            "] " + ChatColor.DARK_GREEN + ChatColor.ITALIC + ChatColor.translateAlternateColorCodes('&', text));
                }
                if (finalCount == speech.size() - 1) {
                    //Last message
                    player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
                    player.removeMetadata("inConversation", WaveNation.get());
                    //Add new mob objective if any, new fight TODO rewards
                }
            }, 20 * sec);
            count++;
        }
    }

    public String getCurrentObjective() {
        return currentObjective;
    }

    public Location getLocation(Player player) {
        World world = player.getWorld();
        Location loc = LocationUtils.stringToLocation(location);
        loc.setWorld(world);
        return loc;
    }

    public Location getCurrentObjectiveLocation(Player player) {
        World world = player.getWorld();
        Location loc = LocationUtils.stringToLocation(locationObjective);
        loc.setWorld(world);
        return loc;
    }


    public BasicDBObject toDBObject() {
        BasicDBObject object = new BasicDBObject();
        object.put("requiredNPCdisplay", requiredNPCdisplay);
        object.put("location", location);
        object.put("radius", radius);
        object.put("name", name);
        object.put("speech", speech);
        object.put("requiredStage", requiredStage);
        object.put("queuedStage", queuedStage);
        object.put("queuedDisplay", queuedDisplay);
        object.put("locationObjective", locationObjective);
        object.put("currentObjective", currentObjective);
        BasicDBList mobList = mythicMobs.stream().map(WaveMob::toDBObject)
                .collect(Collectors.toCollection(() -> new BasicDBList()));

        object.put("mythicMobs", mobList);

        return object;
    }
}
