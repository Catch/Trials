package net.champions.roc.WaveNation.wave.mob;

import com.mongodb.BasicDBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.util.LocationUtils;
import org.bukkit.Location;

public class WaveMob {

    @Getter
    private final String mythicName;

    private final String spawn;

    public WaveMob(BasicDBObject object){
        this.mythicName = object.getString("mythicName");
        this.spawn = object.getString("spawn");
    }

    public Location getLocation(){
        return LocationUtils.stringToLocation(spawn);
    }

    public BasicDBObject toDBObject(){
        BasicDBObject object = new BasicDBObject();
        object.put("mythicName", mythicName);
        object.put("spawn", spawn);
        return object;
    }
}
