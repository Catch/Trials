package net.champions.roc.WaveNation.wave;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.util.LocationUtils;
import net.champions.roc.WaveNation.wave.mob.FightLocation;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Wave {

    @Getter
    private final String name;

    private String iconDisplay , iconLore, location;

    private final int estimatedTime, exp, iconID;

    @Getter
    private final List<Chapter> chapters;

    @Getter
    private final List<FightLocation> fightLocations;

    @Getter
    private final List<Mine> mines;

    public Wave(BasicDBObject object){
        this.name = object.getString("name");
        this.iconDisplay = ChatColor.translateAlternateColorCodes('&', object.getString("iconDisplay"));
        this.iconLore = ChatColor.translateAlternateColorCodes('&', object.getString("iconLore"));
        this.location = object.getString("location");
        this.estimatedTime = object.getInt("estimatedTime");
        this.exp = object.getInt("exp");
        this.iconID = object.getInt("iconID");
        this.chapters = Lists.newArrayList();
        this.fightLocations = Lists.newArrayList();
        this.fightLocations.addAll(((List<BasicDBObject>) object.get("fightLocations")).stream().map(FightLocation::new).collect(Collectors.toList()));
        this.chapters.addAll(((List<BasicDBObject>) object.get("chapters")).stream().map(Chapter::new).collect(Collectors.toList()));

        this.mines = Lists.newArrayList();
        this.mines.addAll(((List<BasicDBObject>) object.get("mines")).stream().map(Mine::new).collect(Collectors.toList()));
    }

    public Location getLocation(){
        return LocationUtils.stringToLocation(location);
    }

    public ItemStack getIcon(){
        ItemStack stack = new ItemStack(iconID);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(iconDisplay);
        meta.setLore(Arrays.asList(iconLore, "", org.bukkit.ChatColor.GRAY + "Estimated Time: " + org.bukkit.ChatColor.YELLOW + estimatedTime,
                org.bukkit.ChatColor.GRAY + "Exp Reward: " +
                        org.bukkit.ChatColor.GOLD + exp));
        stack.setItemMeta(meta);
        return stack;
    }

    public BasicDBObject toDBObject(){
        BasicDBObject object = new BasicDBObject();
        object.put("name", name);
        object.put("iconDisplay", iconDisplay);
        object.put("iconLore", iconLore);
        object.put("estimatedTime", estimatedTime);
        object.put("exp" , exp);
        object.put("iconID" , iconID);
        object.put("location", location);


        BasicDBList chaptersList = chapters.stream().map(Chapter::toDBObject)
                .collect(Collectors.toCollection(() -> new BasicDBList()));

        object.put("chapters", chaptersList);

        BasicDBList fightList = fightLocations.stream().map(FightLocation::toDBObject)
                .collect(Collectors.toCollection(() -> new BasicDBList()));

        object.put("fightLocations", fightList);

        BasicDBList minesList = mines.stream().map(Mine::toDBObject)
                .collect(Collectors.toCollection(() -> new BasicDBList()));

        object.put("mines", minesList);

        return object;
    }
}
