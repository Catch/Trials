package net.champions.roc.WaveNation.wave.npc;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.WaveNation;
import net.champions.roc.WaveNation.player.WavePlayer;
import net.champions.roc.WaveNation.util.ActionBar;
import net.champions.roc.WaveNation.util.LocationUtils;
import net.champions.roc.WaveNation.util.ParticleTrail;
import net.champions.roc.WaveNation.util.Title;
import net.champions.roc.WaveNation.wave.Chapter;
import net.champions.roc.WaveNation.wave.Mine;
import net.champions.roc.WaveNation.wave.Wave;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.trait.LookClose;
import net.citizensnpcs.util.PlayerAnimation;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class WaveNPC {


    private final String type, name, displayName, location;

    @Getter
    private final List<NPCStage> stages;

    private static final Title title;

    static {
        title = new Title(org.bukkit.ChatColor.GOLD + "CONGRATZ!");
        title.setSubtitle(org.bukkit.ChatColor.YELLOW + "You finished the chapter!");
        title.setTimingsToSeconds();
        title.setStayTime(40);
    }

    public WaveNPC(BasicDBObject object) {
        this.type = object.getString("type");
        this.name = object.getString("name");
        this.location = object.getString("defaultLocation");
        this.displayName = ChatColor.translateAlternateColorCodes('&', object.getString("displayName"));
        this.stages = Lists.newArrayList();
        this.stages.addAll(((List<BasicDBObject>) object.get("stages")).stream().map(NPCStage::new).collect(Collectors.toList()));
    }

    public void startStage(Player player, int stagenum) {
        //Talking to an npc already
        if (player.hasMetadata("inConversation")) {
            player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Hey! " + ChatColor.GRAY + "Your currently in a conversation!");
            return;
        }

        NPCStage stage = getStage(stagenum);
        if (stage == null) return;
        NPC npc = getNPC(player);

        //Setup pathing
        npc.getNavigator().setPaused(false);
        npc.getNavigator().getLocalParameters().range(50);
        npc.getNavigator().getDefaultParameters().range(50);
        npc.getNavigator().getLocalParameters().useNewPathfinder(true);
        npc.getNavigator().getDefaultParameters().useNewPathfinder(true);
        HashMap insert = new HashMap();
        insert.put(stage, 0);
        WaveNation.get().getWaveManager().getCurrentNavigators().put(npc.getId(), insert);
        npc.getNavigator().setTarget(stage.getFirstNavigate(player));

        //Cosmetics
        ParticleTrail.sendWorldBUpdate(player, npc);
        player.setMetadata("inConversation", new FixedMetadataValue(WaveNation.get(), "inConversation"));
        stage.playerLookAt(player);

        int count = 0;
        for (String s : stage.getSpeech().keySet()) {
            int sec = Integer.valueOf(s);
            final int finalCount = count;
            Bukkit.getScheduler().runTaskLater(WaveNation.get(), () -> {
                //We have to do more checks because tasks run later times
                if (npc == null || player == null || player.getItemInHand() == null) return;
                if (npc.getBukkitEntity() != null) {
                    PlayerAnimation.ARM_SWING.play((Player) npc.getBukkitEntity());
                }
                if (Bukkit.getPlayerExact(player.getName()) == null || !player.hasMetadata("inConversation")) {
                    return;
                }
                //Speech stuff
                String text = stage.getSpeech().getString(s);
                if (text.contains("<me>")) {
                    String newText = text.replace("<me>", "");
                    player.sendMessage(org.bukkit.ChatColor.GRAY + "[" + ChatColor.AQUA + "You" + ChatColor.GRAY + "] -> [" + ChatColor.AQUA + displayName + ChatColor.GRAY +
                            "] " + ChatColor.DARK_GREEN + ChatColor.ITALIC + ChatColor.translateAlternateColorCodes('&', newText));
                } else if (text.contains("[")) {
                    String resultName = text.substring(text.indexOf("[") + 1, text.indexOf("]"));
                    String newText = text.replace("[" + resultName + "]", "");
                    player.sendMessage(org.bukkit.ChatColor.GRAY + "[" + ChatColor.AQUA + resultName + ChatColor.GRAY + "] -> [" + ChatColor.AQUA + "You" + ChatColor.GRAY +
                            "] " + ChatColor.DARK_GREEN + ChatColor.ITALIC + ChatColor.translateAlternateColorCodes('&', newText));
                } else {
                    player.sendMessage(org.bukkit.ChatColor.GRAY + "[" + ChatColor.AQUA + displayName + ChatColor.GRAY + "] -> [" + ChatColor.AQUA + "You" + ChatColor.GRAY +
                            "] " + ChatColor.DARK_GREEN + ChatColor.ITALIC + ChatColor.translateAlternateColorCodes('&', text));
                }
                //Last stage
                if (finalCount == stage.getSpeech().size() - 1) {
                    //End particles/COsmetics
                    ParticleTrail.removeWorldB(player);
                    player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
                    player.removeMetadata("inConversation", WaveNation.get());

                    //Add new npc objective if any
                    WaveNation.get().getPlayerManager().removeNPCFromQueue(player.getUniqueId(), displayName, stagenum);
                    WaveNation.get().getPlayerManager().addMetNPC(player.getUniqueId(), displayName);
                    WaveNation.get().getPlayerManager().addNPCQueue(player.getUniqueId(), stage.getQueuedDisplay(), stage.getQueuedStage());
                    WaveNation.get().getPlayerManager().updateNPCLob(player.getUniqueId(), displayName, npc.getEntity().getLocation());
                    WavePlayer WavePlayer = WaveNation.get().getPlayerManager().getPlayer(player.getUniqueId());
                    ActionBar.sendActionBar(player, ChatColor.RED + "" + ChatColor.BOLD + "OBJECTIVE: " + ChatColor.AQUA +
                            WavePlayer.getCurrentObjective(), WavePlayer.getCurrentLocationObjective(player));

                    //If its last chapter (field)
                    if (stage.isFinishChapter()) {
                        player.sendMessage(ChatColor.GOLD + "TODO reward here!");
                        title.send(player);
                        WavePlayer.getUnlockedChapters().add(WavePlayer.getCurrentChapter());
                        //Add next one so they can play that
                        for (Chapter chapter : WaveNation.get().getWaveManager().getWave(WavePlayer.getCurrentWave()).getChapters()) {
                            if (!WavePlayer.getUnlockedChapters().contains(chapter.getName())) {
                                WavePlayer.getUnlockedChapters().add(chapter.getName());
                                break;
                            }
                        }

                        //Reset mines
                        Wave Wave = WaveNation.get().getWaveManager().getWave(WaveNation.get()
                                .getPlayerManager().getPlayer(player.getUniqueId()).getCurrentWave());
                        for (Mine m : Wave
                                .getMines()) {
                            m.reset(player);
                        }
                        WavePlayer.setCurrentWave(null);
                        WavePlayer.setCurrentChapter(null);
                        //TP to hub 5 secs later
                        Bukkit.getScheduler().runTaskLater(WaveNation.get(), () -> {
                            WaveNation.get()
                                    .getPlayerManager().savePlayerToDB(player.getUniqueId(), WaveNation.get().getPlayerManager().getPlayer(player.getUniqueId()), aBoolean -> {
                                WaveNation.get().getLilyEssentials().redirectRequest("hub", player);
                            });
                        }, 100L);
                    }
                }
            }, 20 * sec);
            count++;
        }
    }

    public NPC getNPC(Player player) {
        for (NPC npc : CitizensAPI.getNPCRegistry()) {
            if (npc.getEntity().getWorld().getName().equalsIgnoreCase(player.getWorld().getName())
                    && ChatColor.stripColor(npc.getName()).equalsIgnoreCase(org.bukkit.ChatColor.stripColor(displayName))) {
                return npc;
            }
        }
        return null;
    }

    public NPCStage getStage(int stagenum) {
        for (NPCStage stage : stages) {
            if (stage.getStagenum() == stagenum) {
                return stage;
            }
        }
        return null;
    }

    public void spawnAt(Location location) {
        NPC npc = CitizensAPI.getNPCRegistry().createNPC(getType(), displayName);
        LookClose close = new LookClose();
        close.setRange(10);
        npc.spawn(location);
        npc.addTrait(close);
        npc.setFlyable(false);
        npc.setProtected(true);
    }

    public BasicDBObject toDBObject() {
        BasicDBObject object = new BasicDBObject();
        object.put("type", type);
        object.put("name", name);
        object.put("displayName", displayName);

        BasicDBList ss = new BasicDBList();
        for (NPCStage s : stages) {
            ss.add(s.toDBObject());
        }
        object.put("stages", ss);
        return object;
    }

    public EntityType getType() {
        return EntityType.valueOf(type);
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Location getDefaultLocation(String world) {
        Location loc = LocationUtils.stringToLocation(location).clone();
        loc.setWorld(Bukkit.getWorld(world));
        return loc;
    }
}
