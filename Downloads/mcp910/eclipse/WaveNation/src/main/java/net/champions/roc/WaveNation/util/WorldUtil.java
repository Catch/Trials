package net.champions.roc.WaveNation.util;

import net.champions.roc.WaveNation.Settings;
import net.champions.roc.WaveNation.WaveNation;
import net.champions.roc.WaveNation.wave.Chapter;
import net.champions.roc.WaveNation.wave.Wave;
import net.champions.roc.WaveNation.wave.npc.WaveNPC;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WorldUtil {

	public static void createWorlds(List<Wave> Waves) {
		//Delete, Copy, and Load the worlds from a base file
		for (Wave Wave : Waves) {
			for (int x = 1; x < Settings.WORLD_COUNT+1; x++) {
				String base = WaveNation.get().getServer().getWorldContainer().getAbsolutePath() + "/" + Wave.getName() + "-base";
				final File remove = new File(WaveNation.get().getServer().getWorldContainer().getAbsolutePath() + "/" + Wave.getName() + x);
				WorldUtil.delete(remove);
				//Then create world
				WorldUtil.copyDir(new File(base), remove);
				new WorldCreator(Wave.getName() + x).createWorld();
			}
		}

		//We copy the NPC's in a seperate loop after all worlds have been loaded
		for (Wave Wave : Waves) {
			for (int x = 1; x < Settings.WORLD_COUNT+1; x++) {
				for (Chapter chapter : Wave.getChapters()) {
					for (WaveNPC npc : chapter.getNpcs()) {
						Location toSpawn = npc.getDefaultLocation(Wave.getName()+x).clone();
						Bukkit.getWorld(Wave.getName()+x).loadChunk(toSpawn.getChunk());
						npc.spawnAt(toSpawn);
					}
				}
			}
		}
	}

	public static void pasteNPCS(String world){
		Wave Wave = WaveNation.get().getWaveManager().getWave(world.replaceAll("[0-9]", ""));
		for (Chapter chapter : Wave.getChapters()) {
			for (WaveNPC npc : chapter.getNpcs()) {
				Location toSpawn = npc.getDefaultLocation(world).clone();
				npc.spawnAt(toSpawn);
			}
		}
	}

	public static void createWorld(String name) {
		World world = new WorldCreator(name).createWorld();
		Wave Wave = WaveNation.get().getWaveManager().getWave(name.replaceAll("[0-9]", ""));
		WaveNation.get().getDatabaseEngine().getPool().submit(() -> {
			boolean running = true;
			while (running) {
				if (world != null && world.getName() != null) {
					Bukkit.getScheduler().runTask(WaveNation.get(), () -> {
						for (Chapter chapter : Wave.getChapters()) {
							for (WaveNPC npc : chapter.getNpcs()) {
								Location toSpawn = npc.getDefaultLocation(name).clone();
								npc.spawnAt(toSpawn);
							}
						}
					});
					running = false;
				}
			}
		});
	}

	public static boolean delete(File file) {
		if (file.isDirectory())
			for (File subfile : file.listFiles())
				if (!delete(subfile))
					return false;
		if (!file.delete())
			return false;
		return true;
	}

	public static void copyDir(File source, File target) {
		try {
			ArrayList<String> ignore = new ArrayList<String>(Arrays.asList("uid.dat", "session.dat"));
			if (!ignore.contains(source.getName())) {
				if (source.isDirectory()) {
					if (!target.exists())
						target.mkdirs();
					String files[] = source.list();
					for (String file : files) {
						File srcFile = new File(source, file);
						File destFile = new File(target, file);
						copyDir(srcFile, destFile);
					}
				} else {
					InputStream in = new FileInputStream(source);
					OutputStream out = new FileOutputStream(target);
					byte[] buffer = new byte[1024];
					int length;
					while ((length = in.read(buffer)) > 0)
						out.write(buffer, 0, length);
					in.close();
					out.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}