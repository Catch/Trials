package net.champions.roc.WaveNation.database;

public interface Callback<T> {

    void call(T t);
}
