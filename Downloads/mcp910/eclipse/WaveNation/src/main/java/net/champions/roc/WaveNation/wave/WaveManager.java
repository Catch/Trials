package net.champions.roc.WaveNation.wave;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import lombok.Getter;
import net.champions.roc.WaveNation.Settings;
import net.champions.roc.WaveNation.WaveNation;
import net.champions.roc.WaveNation.database.DatabaseEngine;
import net.champions.roc.WaveNation.player.WavePlayer;
import net.champions.roc.WaveNation.util.ActionBar;
import net.champions.roc.WaveNation.util.WorldUtil;
import net.champions.roc.WaveNation.wave.mob.FightLocation;
import net.champions.roc.WaveNation.wave.npc.NPCStage;
import net.champions.roc.WaveNation.wave.npc.WaveNPC;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.ai.event.NavigationCompleteEvent;
import net.citizensnpcs.api.event.CitizensEnableEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.npc.NPC;
import net.elseland.xikage.MythicMobs.API.Bukkit.Events.MythicMobDeathEvent;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Getter
public class WaveManager implements Listener {

    //I may have made maps too complex (efficient, but consider revising)

    private final List<Wave> waves;

    private HashMap<String,Location> lastDeath;

    //Id, stage, current loc index
    private HashMap<Integer,HashMap<NPCStage,Integer>> currentNavigators;

    //Mobs to be beat, playerName, fightlocation, list of mobs uuid
    private HashMap<String,HashMap<FightLocation,List<String>>> currentMobs;

    public WaveManager() {
        this.waves = Lists.newArrayList();
        this.lastDeath = new HashMap<>();
        this.currentNavigators = new HashMap<>();
        this.currentMobs = new HashMap<>();

        //Add all wave templates
        DBCursor cursor = DatabaseEngine.getInstance().getCollection(Settings.Wave_COLL).find();
        while (cursor.hasNext()) {
            Wave Wave = new Wave((BasicDBObject) cursor.next());
            this.waves.add(Wave);
        }

        Bukkit.getPluginManager().registerEvents(this, WaveNation.get());
    }

    @EventHandler
    public void onCitLoad(CitizensEnableEvent e){
        if (WaveNation.get().isHub()) return;
        WorldUtil.createWorlds(waves);
    }

    public void disable() {
        List<NPC> npc = Lists.newArrayList(CitizensAPI.getNPCRegistry().iterator());
        for(int x = 0 ; x < npc.size(); x++){
            CitizensAPI.getNPCRegistry().deregister(npc.get(x));
        }
    }

    @EventHandler
    public void NPCCompleteNavigation(NavigationCompleteEvent e){
        if(!currentNavigators.containsKey(e.getNPC().getId())) return;
        NPC npc = e.getNPC();
        //Increase stage 1 more (next tile to walk to)
        NPCStage stage = currentNavigators.get(npc.getId()).keySet().iterator().next();
        Integer indexLoc = currentNavigators.get(npc.getId()).values().iterator().next();
        indexLoc++;
        System.out.print(indexLoc);
        if(stage.getNavigate((Player) e.getNPC().getBukkitEntity(), indexLoc) == null){
            currentNavigators.remove(npc.getId());
            return;
        }
        //Update map with completed stage
        HashMap insert = new HashMap();
        insert.put(stage, indexLoc);
        currentNavigators.put(npc.getId(),insert);

        //Update new target
        npc.getNavigator().setTarget(stage.getNavigate((Player) e.getNPC().getBukkitEntity(), indexLoc));
    }

    //Reward handling - Apparently not supposed to handle this
    @EventHandler
    public void onMythicKill(MythicMobDeathEvent e) {
        if (e.getKiller() != null && e.getKiller() instanceof Player) {
            if (!currentMobs.containsKey(e.getKiller().getUniqueId().toString())) return;
            Entity mob = e.getEntity();
            String playerUUID = e.getKiller().getUniqueId().toString();
            Player player = Bukkit.getPlayer(UUID.fromString(playerUUID));
            if (player == null || player.getItemInHand() == null) return;

            //Basically make sure they killed all the mobs in the wave, then objective update
            FightLocation location = currentMobs.get(playerUUID).keySet().iterator().next();
            List<String> mobs = currentMobs.get(playerUUID).values().iterator().next();
            if (!mobs.contains(mob.getUniqueId().toString())) return;
            mobs.remove(e.getEntity().getUniqueId().toString());
            if (mobs.size() != 0) return;
            WaveNation.get().getPlayerManager().addNPCQueue(player.getUniqueId(), location.getQueuedDisplay(), location.getQueuedStage());
            WavePlayer WavePlayer2 = WaveNation.get().getPlayerManager().getPlayer(player.getUniqueId());
            ActionBar.sendActionBar(player, ChatColor.RED + "" + ChatColor.BOLD + "OBJECTIVE: " + ChatColor.AQUA +
                    WavePlayer2.getCurrentObjective(), WavePlayer2.getCurrentLocationObjective(player));
            e.getKiller().sendMessage(org.bukkit.ChatColor.GOLD + "Great Kill! - TODO Reward Here");
            currentMobs.remove(e.getEntity().getUniqueId().toString());
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (!WaveNation.get().isHub()) {
            //Possibly a mine
            WavePlayer wavePlayer = WaveNation.get().getPlayerManager().getPlayer(e.getPlayer().getUniqueId());
            Wave Wave = WaveNation.get().getWaveManager().getWave(wavePlayer.getCurrentWave());
            for (Mine m : Wave.getMines()) {
                if (m.getCuboid(e.getPlayer()).hasBlockInside(e.getBlock())) {
                    return;
                }
            }
        }
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e){
       e.setRespawnLocation(lastDeath.get(e.getPlayer().getUniqueId().toString()));
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e){
        if (WaveNation.get().isHub()) return;
        lastDeath.put(e.getEntity().getUniqueId().toString(), e.getEntity().getLocation());
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (WaveNation.get().isHub()) return;
        if (e.getFrom().getBlockX() == e.getTo().getBlockX() && e.getFrom().getBlockY() == e.getTo().getBlockY()
                && e.getFrom().getBlockZ() == e.getTo().getBlockZ()) return;
        for (Wave Wave : waves) {
            //Where fight locations are activated
            for(FightLocation loc : Wave.getFightLocations()) {
                Location clone = loc.getLocation(e.getPlayer());
                if(e.getPlayer().getLocation().distanceSquared(clone) <= loc.getRadius() * loc.getRadius()){
                    loc.activate(e.getPlayer());
                }
            }
        }
    }

    @EventHandler
    public void onNPCClick(NPCRightClickEvent e) {
        if (WaveNation.get().isHub()) return;
        if (getWaveNPC(e.getNPC()) == null) return;
        if (e.getClicker().hasMetadata("inConversation")) {
            e.getClicker().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Hey! " + ChatColor.GRAY + "Your currently in a conversation!");
            return;
        }

        WavePlayer wavePlayer = WaveNation.get().getPlayerManager().getPlayer(e.getClicker().getUniqueId());
        if (wavePlayer == null) {
                e.getClicker().sendMessage(Settings.PREFIX + ChatColor.GRAY + "Looks like we did not find your profile");
                e.getClicker().sendMessage(Settings.PREFIX + ChatColor.GRAY + "Try relogging or contact an admin!");
                return;
            }
            if (wavePlayer.getCurrentWave() == null) {
                e.getClicker().sendMessage(Settings.PREFIX + ChatColor.GRAY + "You didn't start a Wave yet!");
                return;
            }
            //Player exists and we can do checks :D
            WaveNPC npc = getWaveNPC(e.getNPC());
            Integer stageToGet = wavePlayer.getUnMetStage(npc);
            Boolean queuedStage = wavePlayer.hasQueuedNPC(npc.getDisplayName(), stageToGet);
            if(stageToGet == null || queuedStage == null | !queuedStage){
                e.getClicker().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Hey! " + ChatColor.GRAY + "You cannot talk to " + npc.getDisplayName() + " yet!" +
                        " Complete your objectives");
                return;
            }
            npc.startStage(e.getClicker(),stageToGet);
    }

    //No need for thread safety, as these are not being modified, just templates

    public FightLocation getFightLocation(String name){
        for(Wave Wave : waves){
            for(FightLocation location : Wave.getFightLocations()){
                if(location.getName().equalsIgnoreCase(name)){
                    return location;
                }
            }
        }
        return null;
    }

    public Wave getWave(String name){
        for(Wave Wave : waves){
            if(Wave.getName().equalsIgnoreCase(name)){
                return Wave;
            }
        }
        return null;
    }

    public Wave getWave(ItemStack stack) {
        for (Wave Wave : WaveNation.get().getWaveManager().getWaves()) {
            if (ChatColor.stripColor(Wave.getIcon().getItemMeta().getDisplayName()).equalsIgnoreCase(
                    ChatColor.stripColor(stack.getItemMeta().getDisplayName()))) {
                return Wave;
            }
        }
        return null;
    }

    public WaveNPC getWaveNPC(NPC npc) {
        for (Wave Wave : waves) {
            for (Chapter chapter : Wave.getChapters()) {
                for (WaveNPC WaveNPC : chapter.getNpcs()) {
                    if (ChatColor.stripColor(WaveNPC.getDisplayName()).equalsIgnoreCase(ChatColor.stripColor(npc.getName()))) {
                        return WaveNPC;
                    }
                }
            }
        }
        return null;
    }
    public WaveNPC getWaveNPC(String display) {
        for (Wave Wave : waves) {
            for (Chapter chapter : Wave.getChapters()) {
                for (WaveNPC WaveNPC : chapter.getNpcs()) {
                    if (ChatColor.stripColor(WaveNPC.getDisplayName()).equalsIgnoreCase(ChatColor.stripColor(display))) {
                        return WaveNPC;
                    }
                }
            }
        }
        return null;
    }

    public Chapter getChapter(ItemStack stack) {
        for (Wave Wave : WaveNation.get().getWaveManager().getWaves()) {
            for (Chapter chapter : Wave.getChapters()) {
                if (ChatColor.stripColor(chapter.getIconDisplay()).equalsIgnoreCase(
                        ChatColor.stripColor(stack.getItemMeta().getDisplayName()))) {
                    return chapter;
                }
            }
        }
        return null;
    }
}