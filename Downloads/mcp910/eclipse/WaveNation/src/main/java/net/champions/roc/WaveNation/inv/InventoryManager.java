package net.champions.roc.WaveNation.inv;

import lombok.Getter;
import net.champions.roc.WaveNation.Settings;
import net.champions.roc.WaveNation.WaveNation;
import net.champions.roc.WaveNation.network.NetworkManager;
import net.champions.roc.WaveNation.player.WavePlayer;
import net.champions.roc.WaveNation.wave.Chapter;
import net.champions.roc.WaveNation.wave.Wave;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.HashMap;

@Getter
public class InventoryManager implements Listener {

    private ItemStack trigger;

    //Let's cache there inventories (unlocked chapters) to make less db calls
    private HashMap<String, Inventory> cachedMenus;

    public InventoryManager() {
        this.trigger = new ItemStack(Material.BLAZE_ROD);
        ItemMeta meta = trigger.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Quest Selector");
        meta.setLore(Arrays.asList("", ChatColor.AQUA + "Choose what quests you want to play", ChatColor.AQUA + "and get rewards!"));
        this.trigger.setItemMeta(meta);

        this.cachedMenus = new HashMap<>();

        Bukkit.getPluginManager().registerEvents(this, WaveNation.get());
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        //Basic checks, player and correct inventory
        if (event.getInventory() == null) return;
        if (!event.getInventory().getTitle().equalsIgnoreCase("Choose a Trial Chapter")) return;
        if (!(event.getWhoClicked() instanceof Player)) return;
        if (!event.getWhoClicked().getOpenInventory().getItem(event.getSlot()).hasItemMeta()) return;
        event.setCancelled(true);
        event.getWhoClicked().closeInventory();

        //Clicked item is either a wave or chapter, we only listen for chapters
        ItemStack clicked = event.getClickedInventory().getItem(event.getSlot());
        if (WaveNation.get().getWaveManager().getChapter(clicked) != null) {
            //Data 14 means red, not unlocked
            if(clicked.getType() == Material.WOOL && clicked.getData().getData() == 14){
                event.getWhoClicked().sendMessage(ChatColor.GRAY + "You did not unlock that chapter yet!");
                return;
            }
            //No other colors for now, only green
            Chapter chapter = WaveNation.get().getWaveManager().getChapter(event.getClickedInventory().getItem(event.getSlot()));
            WaveNation.get().getPlayerManager().addNPCQueue(event.getWhoClicked().getUniqueId(), chapter.getQueuedDisplay(), chapter.getQueuedStage());
            WavePlayer wavePlayer = WaveNation.get().getPlayerManager().getPlayer(event.getWhoClicked().getUniqueId());
            wavePlayer.setCurrentChapter(chapter.getName());
            wavePlayer.setCurrentWave(chapter.getWaveName());
            //We get the template from the chapter and load it for the player
            chapter.getSaveTemplate().apply(wavePlayer);

            //Save with a confirmation, must happen before sent to instance server
            WaveNation.get().getPlayerManager().savePlayerToDB(event.getWhoClicked().getUniqueId(), wavePlayer, aBoolean -> {
                if (aBoolean) {
                    handleInventoryClick((Player) event.getWhoClicked(), chapter.getWaveName());
                }
            });
        }
    }

    public void handleInventoryClick(Player player, String Wave) {
        //Variables to get the lowest player count on wave-
        String selectedServer = null;
        int count = Integer.MAX_VALUE;
        for (String server : WaveNation.get().getLilyEssentials().getServerSync().getServers()) {
            if (!server.startsWith("wave")) continue;
            if (WaveNation.get().getLilyEssentials().getServerSync().getPlayersOnServer(server).size() <= count) {
                selectedServer = server;
                count = WaveNation.get().getLilyEssentials().getServerSync().getPlayersOnServer(server).size();
            }
        }
        if (selectedServer == null) {
            player.sendMessage(Settings.PREFIX + ChatColor.GRAY + "We could not find an open instance!");
            return;
        }
        //Send a handshake/preparation packet - playeruuid:Wavename
        WaveNation.get().getLilyEssentials().request(NetworkManager.getServer(selectedServer), "wavehandshake", player.getUniqueId().toString() + ":" + Wave);
        player.sendMessage(Settings.PREFIX + ChatColor.GREEN + "We found you a server, sending a request now!");
    }

    private void disconnectPlayer(Player player) {
        cachedMenus.remove(player.getUniqueId().toString());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent e) {
        e.getPlayer().getInventory().clear();
        e.getPlayer().getInventory().addItem(trigger);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent e) {
        disconnectPlayer(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onKick(PlayerKickEvent e) {
        disconnectPlayer(e.getPlayer());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getPlayer().getItemInHand().getType() == Material.BLAZE_ROD) {
            showWaveSelection(e.getPlayer());
        }
    }

    public void showWaveSelection(Player player) {
        if (cachedMenus.containsKey(player.getUniqueId().toString())) {
            player.openInventory(cachedMenus.get(player.getUniqueId().toString()));
        } else {
           WavePlayer WavePlayer = WaveNation.get().getPlayerManager().getPlayer(player.getUniqueId());
                if (Bukkit.getPlayer(player.getName()) != null)
                    player.openInventory(createInventory(player, WavePlayer));
        }
    }

    //New player joins
    public Inventory createInventory(Player player, WavePlayer WavePlayer) {
        Inventory menu = Bukkit.createInventory(null, 54, "Choose a Trial Chapter");
        int count = 0;
        for (Wave wave : WaveNation.get().getWaveManager().getWaves()) {
            menu.setItem(count * 9, wave.getIcon());
            count++;
            int innerCount = count;
            int local = 0;
            for (Chapter chapter : wave.getChapters()) {
                menu.setItem(innerCount, ((local == 0) ? chapter.getUnlockedIcon(WavePlayer) :   chapter.getIcon(WavePlayer)));
                innerCount++;
                local++;
            }
        }
        cachedMenus.put(player.getUniqueId().toString(), menu);
        return menu;
    }
}
