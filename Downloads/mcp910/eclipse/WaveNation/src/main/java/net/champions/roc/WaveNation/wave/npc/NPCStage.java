package net.champions.roc.WaveNation.wave.npc;


import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.util.LocationUtils;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class NPCStage {

    private final String lookAt, currentObjective, locationObjective,queuedDisplay;

    @Getter
    private final int stagenum, queuedStage; //requiredStage;

    @Getter
    private final BasicDBObject speech;

    @Getter
    private final List<String> targetLocations;

    @Getter
    private final boolean finishChapter;

    public NPCStage(BasicDBObject object) {
        this.targetLocations = (List<String>) object.get("targetLocations");
        this.queuedStage = object.getInt("queuedStage");
        this.queuedDisplay = object.getString("queuedDisplay");
        this.stagenum = object.getInt("stagenum");
        this.speech = (BasicDBObject) object.get("speech");
        //this.requiredNPCdisplay = ChatColor.translateAlternateColorCodes('&', object.getString("requiredNPCdisplay"));
        //this.reTalk = object.getBoolean("retalk");
        //this.tpLoc = object.getString("tpLoc");
        this.currentObjective = object.getString("currentObjective");
        this.locationObjective = object.getString("locationObjective");
        this.lookAt = object.getString("lookAt");
        this.finishChapter = (object.get("finishChapter") == null ? false : object.getBoolean("finishChapter"));
    }

    public Location getFirstNavigate(Player player) {
        return getNavigate(player,0);
    }

    public Location getNavigate(Player player, int x) {
        if (targetLocations == null) return null;
        Location loc;
        try {
            loc = LocationUtils.stringToLocation(targetLocations.get(x)).clone();
        } catch (Exception e) {
            return null;
        }
        loc.setWorld(player.getWorld());
        return loc;
    }

    public void playerLookAt(Player player) {
        if (lookAt == null || lookAt.equalsIgnoreCase("") || lookAt.equalsIgnoreCase(" ")) {
            return;
        }
        Location lookLoc = LocationUtils.stringToLocation(lookAt);
        player.teleport(LocationUtils.lookAt(player.getLocation(), lookLoc));
    }

    public String getQueuedDisplay() {
        return queuedDisplay;
    }

    public Location getCurrentObjectiveLocation(Player player) {
        World world = player.getWorld();
        Location loc =  LocationUtils.stringToLocation(locationObjective);
        loc.setWorld(world);
        return loc;
    }

    public String getCurrentObjective() {
        return currentObjective;
    }

    public BasicDBObject toDBObject() {
        BasicDBObject object = new BasicDBObject();
        BasicDBList list = targetLocations.stream().collect(Collectors.toCollection(BasicDBList::new));
        object.put("targetLocations", list);
        object.put("speech", speech);
        object.put("stagenum", stagenum);
        //object.put("requiredNPCdisplay", requiredNPCdisplay);
        //object.put("retalk", reTalk);
        object.put("queuedStage", queuedStage);
        //object.put("tpLoc", tpLoc);
        object.put("queuedDisplay", queuedDisplay);
        object.put("currentObjective", currentObjective);
        object.put("locationObjective", locationObjective);
        return object;
    }
}
