package net.champions.roc.WaveNation.wave;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import lombok.Getter;
import net.champions.roc.WaveNation.player.WavePlayer;
import net.champions.roc.WaveNation.player.savedfields.MetNPC;
import net.champions.roc.WaveNation.player.savedfields.QueuedNPC;
import net.champions.roc.WaveNation.player.savedfields.SavedNPCLoc;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class SaveTemplate {

    //All finals because this is just a template

    private final List<MetNPC> metNPCs;

    private final List<QueuedNPC> queuedNPCs;

    private final List<SavedNPCLoc> NPClocs;

    private final BasicDBObject metFights;

    public SaveTemplate(BasicDBObject object) {
        this.metNPCs = Lists.newArrayList();
        this.queuedNPCs = Lists.newArrayList();
        this.NPClocs = Lists.newArrayList();
        this.NPClocs.addAll(((List<BasicDBObject>) object.get("NPCLocs")).stream().map(SavedNPCLoc::new).collect(Collectors.toList()));
        this.queuedNPCs.addAll(((List<BasicDBObject>) object.get("queuedNPCS")).stream().map(QueuedNPC::new).collect(Collectors.toList()));
        this.metNPCs.addAll(((List<BasicDBObject>) object.get("metNPCS")).stream().map(MetNPC::new).collect(Collectors.toList()));
        this.metFights = (BasicDBObject) object.get("metFights");
    }

    public void apply(WavePlayer player){
        player.setMetNPCs(metNPCs);
        player.setQueuedNPCs(queuedNPCs);
        player.setNPClocs(NPClocs);
        player.setMetFights(metFights);
    }
}
