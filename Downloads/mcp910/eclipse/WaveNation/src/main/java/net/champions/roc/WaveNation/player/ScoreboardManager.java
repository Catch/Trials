package net.champions.roc.WaveNation.player;

import lombok.Getter;
import net.champions.roc.WaveNation.WaveNation;
import net.champions.roc.WaveNation.util.ActionBar;
import net.champions.roc.WaveNation.util.SimpleScoreboard;
import net.champions.roc.WaveNation.wave.Chapter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

@Getter
public class ScoreboardManager implements Listener {

    //Lilypad doesn't have any issues with scoreboard packets like bungee, so we can do this normally

    public ScoreboardManager() {
        Bukkit.getPluginManager().registerEvents(this, WaveNation.get());
    }

    public void update(WavePlayer player, Player bukPlayer) {
        //Called in Async Thread in onJoin
        Chapter chapter = ((player.getCurrentChapter() == null || player.getCurrentWave() == null)? null :  Chapter.findSync(player.getCurrentChapter(), player.getCurrentWave()));
        Bukkit.getScheduler().runTask(WaveNation.get(), () -> {
            SimpleScoreboard scoreboard = new SimpleScoreboard(ChatColor.YELLOW + "" + ChatColor.BOLD + "TRIALNATION");
            scoreboard.add(ChatColor.BLACK.toString(), 7);
            scoreboard.add("Trial: " + ChatColor.LIGHT_PURPLE + player.getCurrentWave(), 6);
            scoreboard.add("Chapter: " + ChatColor.LIGHT_PURPLE + player.getCurrentChapter(), 5);
            scoreboard.add(ChatColor.RED.toString(), 4);
            scoreboard.add("Exp Reward: " + ChatColor.LIGHT_PURPLE + ((chapter == null) ? 0 : chapter.getExp()), 3);
            scoreboard.add("Time Estimate: " + ChatColor.LIGHT_PURPLE + ((chapter == null) ? 0 : chapter.getEstimatedTime()), 2);
            scoreboard.add(ChatColor.DARK_BLUE.toString(), 1);
            scoreboard.update();
            scoreboard.send(bukPlayer);
            ActionBar.sendActionBar(bukPlayer, ChatColor.RED + "" + ChatColor.BOLD + "OBJECTIVE: " + ChatColor.AQUA + player.getCurrentObjective(), player.getCurrentLocationObjective(bukPlayer));
        });
    }
}
