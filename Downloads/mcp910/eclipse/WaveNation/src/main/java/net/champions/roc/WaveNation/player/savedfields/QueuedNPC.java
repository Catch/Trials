package net.champions.roc.WaveNation.player.savedfields;

import com.mongodb.BasicDBObject;
import lombok.Getter;
import lombok.Setter;

public class QueuedNPC {

    @Getter
    private final String displayName;

    @Setter @Getter
    private int stage;

    public QueuedNPC(BasicDBObject object){
        this.displayName = object.getString("displayName");
        this.stage = object.getInt("stage");
    }

    public BasicDBObject toDBObject(){
        BasicDBObject object = new BasicDBObject();
        object.put("displayName", displayName);
        object.put("stage", stage);
        return object;
    }
}
