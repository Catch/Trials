package net.champions.roc.WaveNation;

import com.bobacadodl.lilyessentials.LilyEssentials;
import lombok.Getter;
import net.champions.roc.WaveNation.database.DatabaseEngine;
import net.champions.roc.WaveNation.inv.InventoryManager;
import net.champions.roc.WaveNation.network.NetworkManager;
import net.champions.roc.WaveNation.player.PlayerManager;
import net.champions.roc.WaveNation.player.ScoreboardManager;
import net.champions.roc.WaveNation.util.ActionBar;
import net.champions.roc.WaveNation.util.ParticleTrail;
import net.champions.roc.WaveNation.wave.WaveManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class WaveNation extends JavaPlugin {

    private static WaveNation instance;

    @Getter
    private DatabaseEngine databaseEngine;

    @Getter
    private WaveManager waveManager;

    @Getter
    private PlayerManager playerManager;

    @Getter
    private InventoryManager inventoryManager;

    @Getter
    private boolean hub;

    @Getter
    private NetworkManager networkManager;

    @Getter
    private LilyEssentials lilyEssentials;

    @Getter
    private ScoreboardManager scoreboardManager;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        this.instance = this;
        //If we don't specify a hub, were an instance server by default

        try {
            this.hub = getConfig().getBoolean("isHub");
        } catch (Exception e) {
            this.hub = false;
        }

        //Register the hub and server communications
        this.lilyEssentials = (LilyEssentials) Bukkit.getPluginManager().getPlugin("LilyEssentials");

        //Protocol management
        this.networkManager = new NetworkManager();
        this.lilyEssentials.getConnect().registerEvents(networkManager);

        //Database, use PlayerManager to get players, the callback calls back on main thread (unless told)!!
        this.databaseEngine = new DatabaseEngine();

        //This is where npc clicking is handled, player within location for mob spawning, and Wave/chapter loading from the db
        this.waveManager = new WaveManager();

        //A chapter/Wave GUI for players that opens ONLY for hub
        if (hub) {
            this.inventoryManager = new InventoryManager();
        } else {
            this.scoreboardManager = new ScoreboardManager();

            //Our async packet stuff, not a performance issue via server, may be revised for players with high ping
            Bukkit.getScheduler().runTaskTimerAsynchronously(this, new ActionBar(), 0, 40L);
            Bukkit.getScheduler().runTaskTimerAsynchronously(this, new ParticleTrail(), 0, 5L);
        }
        //A class to get json players async, but the callback calls back on the main thread (unless told).
        this.playerManager = new PlayerManager();
    }

    @Override
    public void onDisable(){
        if(!isHub()){
            //Save all to db
            for(Player p : Bukkit.getOnlinePlayers()){
                playerManager.savePlayerToDB(p.getUniqueId(), playerManager.getPlayer(p.getUniqueId()),
                        aBoolean -> System.out.print("SAVED " + p.getName()));
                lilyEssentials.redirectRequest("hub", p);
            }
        }
        waveManager.disable();
        lilyEssentials.getConnect().unregisterEvents(networkManager);

        this.instance = null;
    }

    public static WaveNation get() {
        return instance;
    }
}
